
const Ci = Components.interfaces;
const Cr = Components.results;
const Cu = Components.utils;

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

const OS = Components.classes["@mozilla.org/observer-service;1"]  
                     .getService(Ci.nsIObserverService);  
const consoleSvc = Components.classes["@mozilla.org/consoleservice;1"]
                             .getService(Ci.nsIConsoleService);

// constructor
XssFilter = function() {
  this.register();
}
// class definition
XssFilter.prototype = {
  classDescription: "CS5231 XSS Filter JavaScript XPCOM Component",
  classID: Components.ID("{519e30d8-7b30-11e1-b6dd-000c29743fdd}"),
  contractID: "@cs5231.nus.edu.sg/xssfilter;1",
  IIDS: [Ci.nsIObserver],
  QueryInterface: XPCOMUtils.generateQI(this.IIDS),

  register: function() {
    OS.addObserver(this.httpObserver, "http-on-modify-request", false);  
    OS.addObserver(this.httpObserver, "http-on-examine-response", false);  
    OS.addObserver(this, "xpcom-shutdown", false);
  },
  unregister: function() {
    OS.removeObserver(this.httpObserver, "http-on-modify-request");
    OS.removeObserver(this.httpObserver, "http-on-examine-response");
    OS.removeObserver(this, "xpcom-shutdown");
  },

  observe : function(subject, topic, data) {
    switch (topic) {
      case "xpcom-shutdown":
        this.unregister(); 
        break;
    }
  }

  httpObserver: {
    observe: function(subject, topic, data) {
      let url,chan;
      chan = subject.QueryInterface(Ci.nsIHttpChannel);
      url = chan.URI.spec;
      switch(topic) {
        case "http-on-modify-request":
          consoleSvc.logStringMessage("req: "+ url);
          break;
        case "http-on-examine-response":
          consoleSvc.logStringMessage("rsp: "+ url);
          break;
      }
    }
  },
}

var components = [XssFilter];
if ("generateNSGetFactory" in XPCOMUtils)
  var NSGetFactory = XPCOMUtils.generateNSGetFactory(components); // FF 4.0+
else
  var NSGetModule  = XPCOMUtils.generateNSGetModule(components); // FF 3.x
